package com.myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.HelloWorld;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext cont = new ClassPathXmlApplicationContext("com/myzee/resouces/spring.xml");
		HelloWorld h = (HelloWorld) cont.getBean("hw");
		h.printMsg();
	}

}
